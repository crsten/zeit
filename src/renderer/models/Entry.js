import nanoid from 'nanoid'

class Entry {
  constructor(data = {}, opts = {}) {
    if (!data) data = {}
    this.id = opts.clone ? data.id : nanoid() || nanoid()
    this.project = data.project || undefined
    this.task = data.task || undefined
    this.start = data.start || new Date()
    this.end = data.end || undefined
  }
}

export default Entry
