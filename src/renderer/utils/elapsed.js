const factors = {
  sec: 1000,
  min: 1000 * 60,
  hour: 1000 * 60 * 60,
}

function extract(value) {
  let hours = Math.floor(value / factors.hour)

  let minuteValue = value % factors.hour
  let minutes = Math.floor(minuteValue / factors.min)

  let secondValue = minuteValue % factors.min
  let seconds = Math.floor(secondValue / factors.sec)

  return {
    hours,
    minutes,
    seconds,
  }
}

function calculate(value, opts = {}) {
  let parts = extract(value)
  let unit = opts.unit || value > factors.hour ? 'Min' : 'Sec'
  let values

  switch (unit) {
    case 'Min':
      values = [parts.hours, parts.minutes]
      break
    case 'Sec':
      values = [parts.minutes, parts.seconds]
      break
  }

  return {
    values,
    unit,
  }
}

function pad(val) {
  if (val < 10 || val.length < 2) return '0' + val
  return val
}

function display(value, opts = {}) {
  let parts = extract(value)
  let result = []

  if (opts.hours !== false && parts.hours) result.push(parts.hours)
  if (opts.minutes !== false) result.push(parts.minutes)
  if (opts.seconds !== false) result.push(parts.seconds)

  return result.map(pad).join(opts.separator || ':')
}

module.exports = {
  calculate,
  pad,
  display,
}
