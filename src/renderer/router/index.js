import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/entry',
      name: 'Entry',
      component: require('@/pages/Entry').default,
    },
    {
      path: '/edit/:id',
      name: 'Edit',
      component: require('@/pages/Edit').default,
      props: true,
    },
    {
      path: '/create',
      name: 'Create',
      component: require('@/pages/Create').default,
    },
    {
      path: '/export',
      name: 'Export',
      component: require('@/pages/Export').default,
    },
    {
      path: '/relation',
      name: 'Relation',
      component: require('@/pages/Relation').default,
    },
    {
      path: '/date/:date',
      name: 'Date',
      component: require('@/pages/Date').default,
      props: true,
    },
    {
      path: '/weekly/:date?',
      name: 'Weekly',
      component: require('@/pages/Weekly').default,
      props: true,
    },
    {
      path: '/settings',
      name: 'Settings',
      component: require('@/pages/Settings').default,
      props: true,
    },
    {
      path: '*',
      redirect: '/weekly',
    },
  ],
})
