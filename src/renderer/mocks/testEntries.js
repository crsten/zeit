import moment from '@/utils/moment'
import Entry from '@/models/Entry'
let w = moment().startOf('w')
export default [
  new Entry({
    start: moment(w)
      .hour(8)
      .minute(30)
      .toDate(),
    end: moment(w)
      .add(1, 'd')
      .hour(10)
      .minute(45)
      .toDate(),
    project: 'Giftcard Shop',
    task: 'Sketching',
  }),
  new Entry({
    start: moment(w)
      .add(1, 'd')
      .hour(8)
      .minute(30)
      .toDate(),
    end: moment(w)
      .add(1, 'd')
      .hour(10)
      .minute(45)
      .toDate(),
    project: 'Giftcard Shop',
    task: 'Frontend',
  }),
]
