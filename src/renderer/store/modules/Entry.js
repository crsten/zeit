import Entry from '../../models/Entry'
import moment from '@/utils/moment'
import MockEntries from '@/mocks/testEntries'
import App from '@/main'

let interval

const options = {
  minLength: 1000 * 60,
  roundTo: 15,
}

const state = {
  elapsed: 0,
  current: null,
  last: null,
  items: [],
}

function splitEachDay(entry) {
  let items = []
  let end = entry.end || new Date()
  let isSameDay = moment(entry.start).isSame(end, 'day')

  if (!isSameDay) {
    items.push({
      ...entry,
      end: moment(entry.start)
        .endOf('day')
        .toDate(),
    })

    return items.concat(
      splitEachDay({
        ...entry,
        end,
        start: moment(entry.start)
          .startOf('day')
          .add(1, 'day'),
      }),
    )
  }

  items.push({
    ...entry,
    end,
  })

  return items
}

const mutations = {
  START(state) {
    let last = new Entry(state.last, { clone: true })
    state.current = new Entry({ project: last.project, task: last.task })
    App.$emit('timer:start')
  },
  STOP(state) {
    App.$emit('timer:stop')
    if (!state.current) return
    if (new Date() - state.current.start < options.minLength) return (state.current = null)

    state.last = new Entry(state.current, { clone: true })
    let items = splitEachDay(state.current)
    state.items = state.items.concat(items)
    state.current = undefined
  },
  UPDATE(state, payload = {}) {
    let { id, ...data } = payload
    let target = id ? state.items.find(i => i.id === id) : state.current

    if (!target) return
    Object.assign(target, data)
  },
  REMOVE(state, payload = {}) {
    let { id, ...data } = payload
    let target = state.items.find(i => i.id === id)

    if (!target) return
    state.items = state.items.filter(i => i !== target)
  },
  CREATE(state, payload) {
    if (!payload) return
    if (new Date() - payload.start < options.minLength) return

    let items = splitEachDay(payload)
    state.items = state.items.concat(items)
  },
  FLUSH(state) {
    state.current = undefined
    state.items = []
  },
  MOCK(state) {
    state.current = undefined
    state.items = MockEntries
  },
  ELAPSED(state, payload) {
    state.elapsed = payload
  },
}

const actions = {
  START({ commit }) {
    commit('STOP')
    commit('START')
  },
  STOP({ commit }) {
    commit('STOP')
  },
  UPDATE({ commit }, payload) {
    commit('UPDATE', payload)
  },
  REMOVE({ commit }, payload) {
    commit('REMOVE', payload)
  },
  CREATE({ commit }, payload) {
    commit('CREATE', payload)
  },
  FLUSH({ commit }) {
    commit('FLUSH')
  },
  MOCK({ commit }) {
    commit('MOCK')
  },
  ELAPSED({ commit }, payload) {
    commit('ELAPSED', payload)
  },
}

const getters = {
  getEntryById: state => id => {
    return state.items.find(i => i.id === id)
  },
  getEntriesByDate: state => date => {
    return state.items.filter(item => moment(item.start).isSame(date, 'd'))
  },
  getWeekEntriesByDate: state => date => {
    return state.items.filter(item => moment(item.start).isSame(date, 'week'))
  },
  getValueByEntry: state => entry => {
    return new Date(entry.end) - new Date(entry.start)
  },
  getValueByDate: (state, getters) => date => {
    return getters.getEntriesByDate(date).reduce((sum, cur) => sum + getters.getValueByEntry(cur), 0)
  },
  getWeekValueByDate: (state, getters) => date => {
    return getters.getWeekEntriesByDate(date).reduce((sum, cur) => sum + getters.getValueByEntry(cur), 0)
  },
  Projects: state => {
    return Array.from(new Set(state.items.map(i => i.project)))
  },
  Tasks: state => {
    return Array.from(new Set(state.items.map(i => i.task)))
  },
  getEntries: (state, getters) => (opts = {}) => {
    return state.items
      .filter(item => {
        let projectQuery = opts.projects.includes(item.project)
        let dateQuery = moment(item.start).isBetween(opts.start, opts.end, 'day', '[]')

        return dateQuery && projectQuery
      })
      .map(item => {
        let duration = getters.getValueByEntry(item)

        item.hours = Math.round((duration / (1000 * 60 * 60)) * 100) / 100
        item.start = moment(item.start).format('DD. MMM YYYY HH:mm')
        item.end = moment(item.end).format('DD. MMM YYYY HH:mm')
        return item
      })
  },
}

export default {
  state,
  mutations,
  actions,
  getters,
}
