const state = {
  settings: null,
}

const mutations = {
  SET(state, payload) {
    state.settings = payload
  },
}

const actions = {
  SET({ commit }, payload) {
    commit('SET', payload)
  },
}

export default {
  namespaced: true,
  state,
  mutations,
  actions,
}
