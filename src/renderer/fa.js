import { library, config } from '@fortawesome/fontawesome-svg-core'

/* Solid icons */
import { faPlay as fasPlay } from '@fortawesome/pro-solid-svg-icons/faPlay'
import { faStop as fasStop } from '@fortawesome/pro-solid-svg-icons/faStop'

library.add(fasStop)
library.add(fasPlay)

/* Regular icons */
// import { faSearch as farSearch } from '@fortawesome/pro-regular-svg-icons/faSearch'

// library.add(farSearch)

/* Light icons */
import { faPlus as falPlus } from '@fortawesome/pro-light-svg-icons/faPlus'
import { faSearch as falSearch } from '@fortawesome/pro-light-svg-icons/faSearch'
import { faTimes as falTimes } from '@fortawesome/pro-light-svg-icons/faTimes'
import { faAngleUp as falAngleUp } from '@fortawesome/pro-light-svg-icons/faAngleUp'
import { faAngleRight as falAngleRight } from '@fortawesome/pro-light-svg-icons/faAngleRight'
import { faAngleLeft as falAngleLeft } from '@fortawesome/pro-light-svg-icons/faAngleLeft'
import { faCheck as falCheck } from '@fortawesome/pro-light-svg-icons/faCheck'
import { faTrash as falTrash } from '@fortawesome/pro-light-svg-icons/faTrash'

library.add(falTrash)
library.add(falCheck)
library.add(falAngleLeft)
library.add(falAngleRight)
library.add(falAngleUp)
library.add(falTimes)
library.add(falSearch)
library.add(falPlus)
