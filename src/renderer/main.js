import '@/styles/main.scss'
import './fa'

import Vue from 'vue'
import axios from 'axios'

import App from './App'
import router from './router'
import store from './store'

import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
Vue.component('FontAwesomeIcon', FontAwesomeIcon)

if (!process.env.IS_WEB) Vue.use(require('vue-electron'))
Vue.http = Vue.prototype.$http = axios
Vue.config.productionTip = false

/* eslint-disable no-new */
const app = new Vue({
  components: { App },
  router,
  store,
  template: '<App/>',
  created() {
    if (this.$store.state.Entry.current) this.$nextTick(() => this.$root.$emit('timer:start'))
  },
}).$mount('#app')

let timer

app.$on('timer:start', () => {
  app.$electron.ipcRenderer.send('timer:start', app.$store.state.Entry.current.start)
  update()
  timer = setInterval(update, 1000)

  function update() {
    let elapsed = new Date().getTime() - new Date(app.$store.state.Entry.current.start).getTime()
    app.$store.dispatch('ELAPSED', elapsed)
  }
})

app.$on('timer:stop', () => {
  clearInterval(timer)
  app.$electron.ipcRenderer.send('timer:stop')
})

app.$electron.ipcRenderer.on('timer:start', function() {
  app.$store.dispatch('START')
  app.$router.push({ name: 'Entry' })
})

app.$electron.ipcRenderer.on('timer:stop', function() {
  app.$store.dispatch('STOP')
  app.$router.push({ name: 'Weekly' })
})

app.$electron.ipcRenderer.on('export', function() {
  app.$router.push({ name: 'Export' })
})

app.$electron.ipcRenderer.on('navigate', function(event, arg) {
  app.$router.push({ name: arg })
})

app.$electron.ipcRenderer.on('settings:set', function(event, arg) {
  app.$store.dispatch('Settings/SET', arg)
})

export default app
