module.exports = {
  autostart: false,
  stopOnLock: true,
  shortcuts: {
    startTimer: 'F13',
    stopTimer: 'F14',
    show: 'F15',
  },
}
