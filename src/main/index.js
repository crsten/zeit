const path = require('path')

import { app, BrowserWindow, Tray, Menu, ipcMain, Notification, globalShortcut } from 'electron'
const settings = require('electron-settings')

const defaultSettings = require('../settingsTemplate')
const Elapsed = require('../renderer/utils/elapsed')

if (process.env.NODE_ENV !== 'development') {
  global.__static = require('path')
    .join(__dirname, '/static')
    .replace(/\\/g, '\\\\')
}

if (process.env.NODE_ENV !== 'development') app.dock.hide()

let mainWindow,
  tray,
  contextMenu,
  timer = { running: false }

const winURL = process.env.NODE_ENV === 'development' ? `http://localhost:9080` : `file://${__dirname}/index.html`

function createWindow() {
  mainWindow = new BrowserWindow({
    height: 650,
    width: 375,
    frame: false,
    fullscreenable: false,
    transparent: true,
    resizable: false,
    show: process.env.NODE_ENV === 'development',
    icon: path.resolve(__dirname, '/build/icons/png/512x512.png'),
  })

  mainWindow.loadURL(winURL)

  mainWindow.on('closed', () => {
    mainWindow = null
  })

  mainWindow.on('blur', () => {
    if (!mainWindow.webContents.isDevToolsOpened()) {
      mainWindow.hide()
    }
  })
}

function createTray() {
  tray = new Tray(__static + '/TrayTemplate.png')
  contextMenu = Menu.buildFromTemplate([
    { label: 'Track your work...', enabled: false },
    {
      label: 'Start',
      type: 'normal',
      icon: __static + '/PlayTemplate.png',
      accelerator: settings.get('shortcuts.startTimer'),
      click() {
        mainWindow.webContents.send('timer:start')
        showWindow()
      },
    },
    {
      label: 'Stop',
      type: 'normal',
      icon: __static + '/StopTemplate.png',
      accelerator: settings.get('shortcuts.stopTimer'),
      click() {
        mainWindow.webContents.send('timer:stop')
      },
    },
    { type: 'separator' },
    {
      label: 'Show',
      type: 'normal',
      accelerator: settings.get('shortcuts.show'),
      click() {
        showWindow()
      },
    },
    {
      label: 'Export',
      type: 'normal',
      click() {
        mainWindow.webContents.send('export')
        showWindow()
      },
    },
    {
      label: 'Settings',
      submenu: [
        {
          id: 'stopOnLock',
          type: 'checkbox',
          label: 'Pause tracking on lock',
          checked: settings.get('stopOnLock'),
          click: toggleStopOnLock,
        },
        {
          id: 'autostart',
          type: 'checkbox',
          label: 'Start on login',
          checked: settings.get('autostart'),
          click: toggleAutostart,
        },
        { type: 'separator' },
        {
          label: 'Shortcuts',
          type: 'normal',
          click() {
            mainWindow.webContents.send('settings:set', settings.getAll())
            mainWindow.webContents.send('navigate', 'Settings')
            showWindow()
          },
        },
      ],
    },
    { type: 'separator' },
    { label: 'Quit', type: 'normal', role: 'quit', accelerator: 'Cmd+Q' },
  ])
  tray.setContextMenu(contextMenu)

  let interval, start
  let setElapsed = () => tray.setTitle(Elapsed.display(new Date() - start))

  ipcMain.on('timer:start', (event, arg) => {
    start = new Date(arg)
    timer.running = true
    setElapsed()
    interval = setInterval(setElapsed, 1000)
  })

  ipcMain.on('timer:stop', (event, arg) => {
    timer.running = false
    clearInterval(interval)
    start = null
    tray.setTitle('')
  })

  ipcMain.on('export', (event, arg) => {
    require('./utils/export')(arg.entries, app.getPath('downloads'))
  })

  ipcMain.on('shortcuts:toggle', (event, arg) => {
    if (!arg) return globalShortcut.unregisterAll()
    return registerShortcuts()
  })

  ipcMain.on('settings:set', (event, arg) => {
    settings.setAll(arg)
    return registerShortcuts()
  })
}

app.on('ready', function() {
  if (!settings.has('autostart')) settings.setAll(defaultSettings)

  createTray()
  createWindow()
  registerShortcuts()

  require('electron').powerMonitor.on('lock-screen', function() {
    if (!settings.get('stopOnLock')) return
    if (timer.running) timer.wasRunning = true
    mainWindow.webContents.send('timer:stop')
  })

  require('electron').powerMonitor.on('unlock-screen', function() {
    if (!settings.get('stopOnLock')) return
    if (!timer.wasRunning) return
    mainWindow.webContents.send('timer:start')

    let notification = new Notification({
      title: 'Zeit',
      body: 'Resuming time tracking after suspending.',
    })
    notification.show()

    timer.wasRunning = false
  })
})

app.on('before-quit', () => {
  mainWindow.webContents.send('timer:stop')
})

app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit()
  }
})

function toggleStopOnLock() {
  let item = contextMenu.getMenuItemById('stopOnLock')
  settings.set('stopOnLock', item.checked)
}

function toggleAutostart() {
  let item = contextMenu.getMenuItemById('autostart')
  settings.set('autostart', item.checked)
  app.setLoginItemSettings({
    openAtLogin: settings.get('autostart'),
  })
}

function registerShortcuts() {
  globalShortcut.unregisterAll()
  let shortcuts = settings.get('shortcuts')

  // Start timer
  if (shortcuts.startTimer)
    globalShortcut.register(shortcuts.startTimer, () => {
      mainWindow.webContents.send('timer:start')
      if (!mainWindow.isVisible()) toggleWindow()
    })

  if (shortcuts.stopTimer)
    globalShortcut.register(shortcuts.stopTimer, () => {
      mainWindow.webContents.send('timer:stop')
    })

  if (shortcuts.show) globalShortcut.register(shortcuts.show, toggleWindow)
}

const getWindowPosition = () => {
  const windowBounds = mainWindow.getBounds()
  const trayBounds = tray.getBounds()

  const x = Math.round(trayBounds.x + trayBounds.width / 2 - windowBounds.width / 2)
  const y = Math.round(trayBounds.y + trayBounds.height + 10)

  return { x: x, y: y }
}

const toggleWindow = () => {
  if (mainWindow.isVisible()) {
    mainWindow.hide()
  } else {
    showWindow()
  }
}

const showWindow = () => {
  const position = getWindowPosition()
  mainWindow.setPosition(position.x, position.y, false)
  mainWindow.show()
  mainWindow.focus()
}

/**
 * Auto Updater
 *
 * Uncomment the following code below and install `electron-updater` to
 * support auto updating. Code Signing with a valid certificate is required.
 * https://simulatedgreg.gitbooks.io/electron-vue/content/en/using-electron-builder.html#auto-updating
 */

/*
import { autoUpdater } from 'electron-updater'

autoUpdater.on('update-downloaded', () => {
  autoUpdater.quitAndInstall()
})

app.on('ready', () => {
  if (process.env.NODE_ENV === 'production') autoUpdater.checkForUpdates()
})
 */
