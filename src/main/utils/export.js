const Json2csvParser = require('json2csv').Parser
const fs = require('fs')
const fields = [
  {
    label: 'Start',
    value: 'start',
  },
  {
    label: 'End',
    value: 'end',
  },
  {
    label: 'Project',
    value: 'project',
  },
  {
    label: 'Task',
    value: 'task',
  },
  {
    label: 'Hours',
    value: 'hours',
  },
]

const json2csvParser = new Json2csvParser({ fields })

module.exports = function(value, path, format) {
  const csv = json2csvParser.parse(value)
  fs.writeFileSync(path + '/zeit-export_' + new Date().getTime() + '.csv', csv, { encoding: 'UTF-8', flag: 'w' })
}
